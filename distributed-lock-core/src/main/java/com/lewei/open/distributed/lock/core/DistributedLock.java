package com.lewei.open.distributed.lock.core;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Distributed lock interface
 *
 * @author <a href="mailto:yins_emial@foxmail.com">yins</a>
 * @version 0.1
 * @date 2021-11-08 14:10
 */
public interface DistributedLock<K extends Serializable> {

    /**
     * lock of by key
     *
     * @param key lock key
     * @return
     */
    boolean lock(K key);

    /**
     * lock of by key
     *
     * @param key      lock key
     * @param duration expiration
     * @param timeUnit {@code duration} {@link TimeUnit}
     * @return
     */
    boolean lock(K key, long duration, TimeUnit timeUnit);

    /**
     * releases the lock
     *
     * @param key lock key
     */
    void unlock(K key);
}
