package com.lewei.open.distributed.lock.zk;


import com.lewei.open.distributed.lock.core.DistributedLock;

import java.util.concurrent.TimeUnit;

/**
 * {@link DistributedLock} by redis
 *
 * @author <a href="mailto:yins_emial@foxmail.com">yins</a>
 * @date 2021-11-08 18:01
 */
public class ZookeeperDistributedLock implements DistributedLock<String> {

    private static final String ROOT_PATH = "/distributed/locks";


    @Override
    public boolean lock(String key) {
        return false;
    }

    @Override
    public boolean lock(String key, long duration, TimeUnit timeUnit) {
        return false;
    }

    @Override
    public void unlock(String key) {

    }
}
